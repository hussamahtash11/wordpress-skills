<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fcy7JSqjJFfVdl67gMLAgT/p2DBqYIA22mGVbmfhKe/8U2UqRb1l8utatUAcShoFB3VnKmuaywpG9K2QpQ6snQ==');
define('SECURE_AUTH_KEY',  'mlEKv9+gtxr5Lxe00Npny86FQXXZIBMVArxNTWvqJupCTI7PYQXLBr/B7ZvDtlr8JjacEjP7iyqQsYkaNAR2IA==');
define('LOGGED_IN_KEY',    '+yy+Qyd9wAIi3LOI4sHHILMqVO7HJ1hTR5hEMx9cpeythxwX3A+3tgEDUB09zIcMYrlIVo6X7dRS+ekD7ROdnw==');
define('NONCE_KEY',        '72m3s7cvP6MwfbKgDlD//xlGmZir+6yHbVNssnuHUwYKeOWWGZTTT4kaG8D5o+xUVLgeqCdFvzGYYRjHivfdfA==');
define('AUTH_SALT',        '1WL6TIYUocrY7LbjFgyUExV6B2Vsfj9mScM4rcO3JJ0PD66PPvJ+JpIBUgTOyjCU5+gRqaB5UP1d19Tb+aQ/uQ==');
define('SECURE_AUTH_SALT', 'AewHp/1iIKQ/V5yZZPLPz+COmkqg3TcCf8dTI1fCuONnNRnpKPRdX9CXLmELx6d7Rkt8bhPAqPTbWXdaj+ZTFg==');
define('LOGGED_IN_SALT',   'o371aPut+tLwyYCl9z5kRHqIO/LJ8SVCUgo3UUj6qe1XqLOeSp/cPFvAqGWALSRiLCR9O9BR7uBgF9j5pVy7IA==');
define('NONCE_SALT',       '8Lqus1Ta0Br5+ZhWVF6DCc4u5NifOo/d/eCPcCmdPWyGbzRDc87FXxGStgQKZxoT0VQAhPQobp7hNB+ev3L36Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
